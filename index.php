<?php

  require_once 'animal.php';
  require_once 'frog.php';
  require_once 'ape.php';

  $sheep = new Animal("shaun");
  echo "SOAL 1 <br>";
  echo $sheep->name . "<br>";
  echo $sheep->legs . "<br>";
  echo $sheep->cold_blooded . "<br><br>";

  echo "SOAL 2 <br>";
  $sungokong = new Ape("kera sakti");
  echo $sungokong->name . "<br>";
  echo $sungokong->legs . "<br>";
  $sungokong->yell(); // "Auooo"

  $kodok = new Frog("buduk");
  echo $kodok->name . "<br>";
  echo $kodok->legs . "<br>";
  $kodok->jump(); // "hop hop"

?>
